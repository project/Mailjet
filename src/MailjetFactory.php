<?php

namespace Drupal\mailjet;

use Drupal\Core\Config\ConfigFactoryInterface;
use Mailjet\Client;

/**
 * This class provide mailjet factory.
 */
class MailjetFactory {
  /**
   * Configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $mailjetConfig;

  /**
   * Constructs MailjetFactory object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->mailjetConfig = $configFactory->get(MailjetHandlerInterface::CONFIG_NAME);
  }

  /**
   * Constructs MailjetFactory object.
   *
   * @param bool $skip_active_status
   *   (optional) Skip active status check. Defaults to FALSE.
   *
   * @return \Mailjet\Client|null
   *   Mailjet client.
   */
  public function create(bool $skip_active_status = FALSE): ?Client {
    $mailjetActive = $skip_active_status ?: $this->mailjetConfig->get('mailjet_active');
    $mailjetApikey = $this->mailjetConfig->get('mailjet_username');
    $mailjetApiSecret = $this->mailjetConfig->get('mailjet_password');

    if (empty($mailjetActive) || empty($mailjetApikey) || empty($mailjetApiSecret)) {
      return NULL;
    }

    $mjClient = new Client($mailjetApikey, $mailjetApiSecret);
    $mjClient->addRequestOption(CURLOPT_USERAGENT, 'drupal-mailjet-module');
    $mjClient->addRequestOption('headers', ['User-Agent' => 'drupal-mailjet-module']);
    $mjClient->setConnectionTimeout(10);

    if (
          (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'off')
          || empty($_SERVER['HTTPS'])
          || $_SERVER['SERVER_PORT'] != 443
      ) {
      $mjClient->setSecureProtocol(FALSE);
    }

    return $mjClient;
  }

}
