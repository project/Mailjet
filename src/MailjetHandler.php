<?php

namespace Drupal\mailjet;

use Mailjet\Resources;

/**
 * Implement MailjetHandler interface.
 */
class MailjetHandler implements MailjetHandlerInterface {
  /**
   * Mailjet client.
   *
   * @var \Mailjet\Client
   */
  protected $mailjetClient;

  /**
   * {@inheritdoc}
   *
   * @return void
   *   Return array with result or false.
   */
  public function __construct(MailjetFactory $mailjetClient) {
    $this->mailjetClient = $mailjetClient->create();
  }

  /**
   * Gets Mailjet default list.
   *
   * @return array|null
   *   Return array with result or false.
   */
  public function getMailjetDefaultList(): ?array {
    // Trying to get Mailjet contact list by its name.
    $contactList = $this->getMailjetContactListByName(self::DEFAULT_CONTACT_LIST_NAME);

    // A Mailjet contact list is available on Mailjet side.
    if (!empty($contactList) && !empty($contactList[0])) {
      return $contactList[0];
    }

    // Trying to create a new Mailjet contact list.
    $newContactList = $this->createMailjetContactList(self::DEFAULT_CONTACT_LIST_NAME);
    if (!empty($newContactList) && !empty($newContactList[0])) {
      return $newContactList[0];
    }

    // We can't either retrieve the existing or create a new Mailjet contact
    // list.
    return NULL;
  }

  /**
   * {@inheritdoc}
   *
   * @return array|null
   *   Return array with result or null.
   */
  public function getMailjetContactLists($limit = 0): ?array {
    $filters = [
      'Limit' => $limit,
      'Sort' => 'Name ASC',
    ];
    $response = $this->mailjetClient->get(Resources::$Contactslist, ['filters' => $filters]);

    if ($response->success()) {
      return $response->getData();
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getMailjetContactListByName(string $name): ?array {
    $filters = [
      'Name' => $name,
    ];
    $response = $this->mailjetClient->get(Resources::$Contactslist, ['filters' => $filters]);

    if ($response->success() && $response->getCount() > 0) {
      return $response->getData();
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getMailjetContactProperties(int $limit = 0): ?array {
    $filters = [
      'Limit' => $limit,
    ];
    $response = $this->mailjetClient->get(Resources::$Contactmetadata, ['filters' => $filters]);

    if ($response->success()) {
      return $response->getData();
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   *
   * @return array|null
   *   Return array with result or null.
   */
  public function createMailjetContactList($listName): ?array {
    if (empty($listName)) {
      return NULL;
    }

    $body = [
      'Name' => $listName,
    ];
    $response = $this->mailjetClient->post(Resources::$Contactslist, ['body' => $body]);

    if ($response->success()) {
      return $response->getData();
    }

    return NULL;
  }

  /**
   * Get contact properties.
   *
   * @return array|null
   *   Return array with response or null.
   */
  public function getContactProperties(): ?array {
    $filters = [
      'limit' => 0,
      'Sort' => 'Name ASC',
    ];
    $response = $this->mailjetClient->get(Resources::$Contactmetadata, ['filters' => $filters]);

    if ($response->success()) {
      return $response->getData();
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   *
   * @return array|null
   *   Return array with result or null.
   */
  public function createMailjetContactProperty($name, $type = "str"): ?array {
    if (empty($name)) {
      return NULL;
    }

    // Name: the name of the custom data field.
    // DataType: the type of data that is being stored
    // (this can be either a str, int, float or bool).
    // NameSpace: this can be either static or historic.
    $body = [
      'Datatype' => $type,
      'Name' => $name,
      'NameSpace' => "static",
    ];
    $response = $this->mailjetClient->post(Resources::$Contactmetadata, ['body' => $body]);

    if ($response->success()) {
      return $response->getData();
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   *
   * @return array|null
   *   Return array with result or null.
   */
  public function updateMailjetContactProperty($id, $name, $type = "str"): ?array {
    if (empty($name)) {
      return NULL;
    }

    // Name: the name of the custom data field.
    // DataType: the type of data that is being stored
    // (this can be either a str, int, float or bool).
    // NameSpace: this can be either static or historic.
    $body = [
      'Datatype' => $type,
      'Name' => $name,
      'NameSpace' => "static",
    ];
    $response = $this->mailjetClient->put(
          Resources::$Contactmetadata,
          ['id' => $id, 'body' => $body]
      );

    if ($response->success()) {
      return $response->getData();
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function syncMailjetContacts(int $contactListId, array $contacts, string $action = 'addforce'): ?array {
    $body = [
      'Action' => $action,
      'Contacts' => $contacts,
    ];
    $response = $this->mailjetClient->post(
          Resources::$ContactslistManagemanycontacts,
          ['id' => $contactListId, 'body' => $body]
      );

    if ($response->success()) {
      return $response->getData();
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   *
   * @return array|null
   *   Return array with result or null.
   */
  public function syncMailjetContact($contactListId, array $contact, $action = 'addforce'): ?array {
    $body = [
      'Action' => $action,
      'Email' => $contact['Email'],
    ];

    if (!empty($contact['Properties'])) {
      $body['Properties'] = $contact['Properties'];
    }

    $response = $this->mailjetClient->post(
          Resources::$ContactslistManagecontact,
          ['id' => $contactListId, 'body' => $body]
      );

    if ($response->success()) {
      return $response->getData();
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   *
   * @return array|null
   *   Return array with result or null.
   */
  public function createApiToken(array $params): ?array {
    $response = $this->mailjetClient->post(Resources::$Apitoken, ['body' => $params]);
    if ($response->success()) {
      return $response->getData();
    }

    return NULL;
  }

}
