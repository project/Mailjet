<?php

namespace Drupal\mailjet\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 *
 */
class MailjetMyAccountController extends ControllerBase {

  /**
   * @return false|null
   */
  public function redirectMyProfile() {
    define('IFRAME_URL', 'https://app.mailjet.com/');
    return mailjetExternalLinkRedirect(IFRAME_URL . 'account');
  }//end redirectMyProfile()

}//end class
