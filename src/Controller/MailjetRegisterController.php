<?php

namespace Drupal\mailjet\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 *
 */
class MailjetRegisterController extends ControllerBase {

  /**
   */
  public function redirectRegister() {
    define('IFRAME_URL', 'https://app.mailjet.com/');
    return mailjetExternalLinkRedirect(IFRAME_URL . 'signup?aff=drupal-3.0');
  }//end redirectRegister()

}//end class
