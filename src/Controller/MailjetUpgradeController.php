<?php

namespace Drupal\mailjet\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 *
 */
class MailjetUpgradeController extends ControllerBase {

  /**
   * @return false|null
   */
  public function redirectUpgrade() {
    define('IFRAME_URL', 'https://app.mailjet.com/');
    return mailjetExternalLinkRedirect(IFRAME_URL . 'pricing');
  }//end redirectUpgrade()

}//end class
