<?php

namespace Drupal\mailjet\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

/**
 * The controller for Mailjet admin panel page.
 */
class MailjetAdminPanelController extends ControllerBase {

  /**
   * Outputs Mailjet admin panel page render array.
   *
   * @return array
   *   A render array.
   */
  public function content() {
    $content = [
      [
        'url' => Url::fromUri('https://app.mailjet.com/stats'),
        'title' => $this->t('Dashboard'),
        'description' => $this->t('Click here to gain insight into your user engagement (Clicks, Opens, Geographies, User Agents, Unsubscribes, etc.)'),
      ],
      [
        'url' => Url::fromUri('https://app.mailjet.com/account'),
        'title' => $this->t('My profile'),
        'description' => $this->t('Click here to change account settings, configure SPF and DKIM for optimized deliverability, etc.'),
      ],
      [
        'url' => Url::fromUri('https://app.mailjet.com/campaigns'),
        'title' => $this->t('Campaigns'),
        'description' => $this->t('Click here to access the Mailjet drag-and-drop newsletter tool, templates and more for your marketing campaigns.'),
      ],
      [
        'url' => Url::fromUri('https://app.mailjet.com/contacts'),
        'route_name' => 'mailjet-panel.list',
        'title' => $this->t('Contacts'),
        'description' => $this->t('Click here to access the Mailjet contact lists.'),
      ],
      [
        'url' => Url::fromUri('https://app.mailjet.com/account/subscription'),
        'title' => $this->t('Upgrade'),
        'description' => $this->t('Click here to change/upgrade your current plan.'),
      ],
    ];

    return [
      'content' => [
        '#theme' => 'admin_block_content',
        '#content' => $content,
      ],
    ];
  }//end content()

}//end class
