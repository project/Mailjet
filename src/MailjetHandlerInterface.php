<?php

namespace Drupal\mailjet;

/**
 * Interface for mailjet handler.
 */
interface MailjetHandlerInterface {
  public const CONFIG_NAME = 'mailjet.settings';

  /**
   * The default name for the contact list (if no lists exist on MailJet side).
   */
  public const DEFAULT_CONTACT_LIST_NAME = 'Drupal contact list';

  /**
   * Get contact list for Mailjet.
   *
   * @param int $limit
   *   Contain limit of list item.
   */
  public function getMailjetContactLists($limit = 0);

  /**
   * Get mailjet contacts by name.
   *
   * @param string $name
   *   Name of contact.
   *
   * @return array|null
   *   Return an array with contact list or null.
   */
  public function getMailjetContactListByName(string $name): ?array;

  /**
   * Get contact properties.
   *
   * @param int $limit
   *   Contain limit of list item.
   *
   * @return array|null
   *   Return array with the result or null.
   */
  public function getMailjetContactProperties(int $limit = 0): ?array;

  /**
   * Create response with contacts list.
   *
   * @param string $listName
   *   Contain list name.
   */
  public function createMailjetContactList($listName);

  /**
   * Get contact properties.
   */
  public function getContactProperties();

  /**
   * Create response with contact properties.
   *
   * @param string $name
   *   Contain contact name.
   * @param string $type
   *   Contain contact type.
   */
  public function createMailjetContactProperty($name, $type = 'str');

  /**
   * Create response with update of contact properties.
   *
   * @param int $id
   *   Contain contact id.
   * @param string $name
   *   Contain contact name.
   * @param string $type
   *   Contain contact type.
   */
  public function updateMailjetContactProperty($id, $name, $type = 'str');

  /**
   * Sync of Contacts.
   *
   * @param int $contactListId
   *   Contain contact list id.
   * @param array $contacts
   *   Contain contacts.
   * @param string $action
   *   Contain action name.
   *
   * @return array|null
   *   Return array with result or null.
   */
  public function syncMailjetContacts(int $contactListId, array $contacts, string $action = 'addforce'): ?array;

  /**
   * Sync of Contact.
   *
   * @param int $contactListId
   *   Contain contact list id.
   * @param array $contact
   *   Contain contacts.
   * @param string $action
   *   Contain action name.
   */
  public function syncMailjetContact($contactListId, array $contact, $action = 'addforce');

  /**
   * Crate api key.
   *
   * @param array $params
   *   Contain params for creating api token.
   */
  public function createApiToken(array $params);

}//end interface
