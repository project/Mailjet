<?php

namespace Drupal\mailjet;

/**
 * Interface for Mailjet properties sync service.
 */
interface MailjetPropertiesSyncInterface {

  /**
   * Syncs Drupal user fields with Mailjet properties.
   *
   * Gets the user fields and compare them with existing Mailjet properties.
   * If field settings were changed in Drupal, updates property in Mailjet.
   * If field was created in Drupal, creates property in Mailjet.
   */
  public function syncMailjetProperties();

}
