<?php

namespace Drupal\mailjet\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 *
 * @package Drupal\mailjet\EventSubscriber
 */
class InitSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [KernelEvents::REQUEST => ['onEvent', 0]];
  }//end getSubscribedEvents()

  /**
   * {@inheritdoc}
   */
  public function onEvent() {
  }//end onEvent()

}//end class
