<?php

namespace Drupal\mailjet\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mailjet\MailjetHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Config form for subscribe email.
 */
class SubscribeEmailForm extends ConfigFormBase {
  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * MailjetHandler service.
   *
   * @var \Drupal\mailjet\MailjetHandlerInterface
   */
  protected $mailjetHandler;

  /**
   * SubscribeEmailForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\mailjet\MailjetHandlerInterface $mailjetHandler
   *   The mailjet handler service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entityTypeManager,
    MailjetHandlerInterface $mailjetHandler,
  ) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entityTypeManager;
    $this->mailjetHandler    = $mailjetHandler;
  }//end __construct()

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('config.factory'),
          $container->get('entity_type.manager'),
          $container->get('mailjet.handler')
      );
  }//end create()

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'subscribe_admin_form';
  }//end getFormId()

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['config.subscribe_form'];
  }//end getEditableConfigNames()

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];
    if (!empty($_GET['list'])) {
      $list_id = $_GET['list'];
    }
    if (!empty($_GET['sec_code'])) {
      $sec_code_email = base64_decode($_GET['sec_code']);
    }
    if (!empty($_GET['properties'])) {
      $properties = json_decode(base64_decode($_GET['properties']));
    }
    if (!empty($_GET['others'])) {
      $form_hidden_id = $_GET['others'];
    }
    else {
      return FALSE;
    }
    $signup_form = $this->entityTypeManager->getStorage('mailjet_subscription_form')->load($form_hidden_id);

    $contact = [
      'Email' => $sec_code_email,
    ];

    // If we have any properties we clean the `signup-`
    // part from the name and prepare them to sync to Mailjet
    // Note that the `$properties` is Object not Array.
    if (!empty($properties)) {
      $propertiesClean = [];
      foreach ($properties as $key => $value) {
        if (stristr($key, 'signup-')) {
          $keyClean = str_ireplace('signup-', '', $key);
          switch (mailjet_get_propertiy_type($keyClean)) {
            case 'int':
              $propertiesClean[$keyClean] = (int) $value;
              break;

            case 'str':
              $propertiesClean[$keyClean] = (string) $value;
              break;

            case 'float':
              $propertiesClean[$keyClean] = (float) $value;
              break;

            case 'datetime':
              $datetime = \DateTime::createFromFormat('d-m-Y', $value);
              if ($datetime instanceof \DateTime) {
                $propertiesClean[$keyClean] = $datetime->format(\DateTime::RFC3339);
              }
              break;

            case 'bool':
              if (strtoupper($value) == 'TRUE') {
                $propertiesClean[$keyClean] = TRUE;
              }
              else {
                $propertiesClean[$keyClean] = FALSE;
              }
              break;
          }//end switch
        }//end if
      }//end foreach
      $contact['Properties'] = $propertiesClean;
    }//end if

    // Add new email.
    $response = $this->mailjetHandler->syncMailjetContact($list_id, $contact);

    if (FALSE != $response) {
      if (!empty($signup_form->success_message_subsribe)) {
        $this->messenger()->addStatus(t($signup_form->success_message_subsribe));
      }
      else {
        $this->messenger()->addStatus(t('You have successfully subscribed to Mailjet contact list! Thank you!'));
      }
    }

    return $form;
  }//end buildForm()

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }//end validateForm()

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }//end submitForm()

}//end class
