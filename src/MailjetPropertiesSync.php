<?php

namespace Drupal\mailjet;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Mailjet properties sync service.
 */
class MailjetPropertiesSync implements MailjetPropertiesSyncInterface {
  /**
   * Mailjet handler service.
   *
   * @var \Drupal\mailjet\MailjetHandler
   */
  protected $mailjetHandler;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The user entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $userStorage;

  /**
   * Constructs new MailjetPropertiesSync object.
   *
   * @param \Drupal\mailjet\MailjetHandler $mailjetHandler
   *   Mailjet handler service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(MailjetHandler $mailjetHandler, AccountInterface $current_user, EntityTypeManagerInterface $entityTypeManager) {
    $this->mailjetHandler = $mailjetHandler;
    $this->currentUser = $current_user;
    $this->userStorage = $entityTypeManager->getStorage('user');
  }

  /**
   * {@inheritdoc}
   */
  public function syncMailjetProperties() {
    // Add 'Name' and 'Datatype' properties as default.
    $userFields = [
      [
        'Name' => 'name',
        'Datatype' => 'str',
      ],
    ];

    // Gets field of the current user. We take it only for the properties sync,
    // that's why it's possible to get the current user.
    $user = $this->userStorage->load($this->currentUser->id());
    foreach ($user as $propertyName => $propertyValue) {
      if (strpos($propertyName, 'field_') !== FALSE) {
        if (empty($fieldDefinition = $user->getFieldDefinition($propertyName))) {
          continue;
        }

        $userFields[] = [
          'Name' => str_replace('field_', '', $propertyName),
        // Gets field type that fits for the Mailjet.
          'Datatype' => $this->getMailjetFiledType($fieldDefinition->getType()),
        ];
      }
    }

    // Get existing contact properties from MailJet.
    $properties = [];
    if ($response = $this->mailjetHandler->getContactProperties()) {
      foreach ($response as $property) {
        $properties[$property['Name']] = (array) $property;
      }
    }

    // Sync Drupal fields to Mailjet properties.
    foreach ($userFields as $field) {
      // Update Mailjet property if:
      if (
            // Field already exists in Mailjet.
            isset($properties[$field['Name']])
            // And field type was changed.
            && $field['Datatype'] != $properties[$field['Name']]['Datatype']
        ) {
        $updateResponse = $this->mailjetHandler->updateMailjetContactProperty(
              $properties[$field['Name']]['ID'],
              $field['Name'],
              $field['Datatype'],
          );

        if (!empty($updateResponse)) {
          // Add it here, so the next loop it won't be handled again.
          $properties[$field['Name']] = $field;
        }
      }
      // Create Mailjet contact property if not exist.
      if (!isset($properties[$field['Name']])) {
        $insertResponse = $this->mailjetHandler->createMailjetContactProperty(
              $field['Name'],
              $field['Datatype'],
          );

        if (!empty($insertResponse)) {
          // Add it here, so the next loop it won't be handled again.
          $properties[$field['Name']] = $field;
        }
      }
    }
  }

  /**
   * Gets the Mailjet field type.
   *
   * Mapping for the Mailjet field types according to the Drupal field type.
   *
   * @param string $currentFieldType
   *   Drupal field type.
   *
   * @return string
   *   Returns Mailjet field type.
   *
   * @see https://dev.mailjet.com/email/reference/contacts/contact-properties/
   */
  public function getMailjetFiledType(string $currentFieldType): string {
    if (in_array($currentFieldType, ['integer', 'number_integer', 'timestamp'])) {
      return 'int';
    }

    if (
        in_array(
              $currentFieldType,
              [
                'float',
                'number_float',
                'decimal',
                'number_decimal',
                'commerce_price',
              ]
          )
      ) {
      return 'float';
    }

    if (in_array($currentFieldType, ['boolean', 'list_boolean'])) {
      return 'bool';
    }

    return $currentFieldType == 'datetime' ? 'datetime' : 'str';
  }

}
