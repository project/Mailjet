<?php

namespace Drupal\mailjet\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\Exception\BadResponseException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Process a queue of the Mailjet contact sync.
 *
 * @QueueWorker(
 *   id = "sync_mailjet_contact",
 *   title = @Translation("Mailjet contact sync"),
 *   cron = {"time" = 60}
 * )
 */
class MailjetSyncContactQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {
  use StringTranslationTrait;

  /**
   * Mailjet handler service.
   *
   * @var \Drupal\mailjet\MailjetHandler
   */
  protected $mailjetHandler;

  /**
   * Logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Mailjet properties sync service.
   *
   * @var \Drupal\mailjet\MailjetPropertiesSyncInterface
   */
  protected $mailjetPropertiesSyncService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->mailjetHandler = $container->get('mailjet.handler');
    $instance->logger = $container->get('logger.factory')->get('mailjet_messages');
    $instance->mailjetPropertiesSyncService = $container->get('mailjet.properties_sync');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if (empty($data['contact']) || empty($action = $data['action'])) {
      return;
    }

    // Gets Mailjet default list ID.
    $mailjetList = $this->mailjetHandler->getMailjetDefaultList();
    $listId = $mailjetList['ID'] ?? NULL;
    if (!$listId) {
      return;
    }

    // Syncs Mailjet properties.
    $this->mailjetPropertiesSyncService->syncMailjetProperties();

    try {
      $this->mailjetHandler->syncMailjetContact($listId, $data['contact'], $action == 'remove' ? 'remove' : 'addforce');
      $this->logger->notice(
        $this->t(
          'Contact synced successfully. List ID: @listID, action: @action',
          [
            '@listID' => $listId,
            '@action' => $action,
          ]
        )
      );
    }
    catch (BadResponseException $e) {
      $this->logger->error(
        $this->t(
          'Contact sync error. List ID: @listID, action: @action. Message: @msg.',
          [
            '@listID' => $listId,
            '@action' => $action,
            '@msg' => $e->getMessage(),
          ]
        )
          );
    }
  }

}
