<?php

namespace Drupal\mailjet;

/**
 *
 */
class MailjetException extends \Exception {

  /**
   * @param string $string
   */
  public function __construct(string $string) {
    parent::__construct($string);
  }//end __construct()

}//end class
