<?php

namespace Drupal\Tests\mailjet\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Mailjet\Client;

/**
 * Tests core API Mailjet functionality.
 *
 * @group mailjet
 */
class ApiClientTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['mailjet'];

  /**
   * Tests that the test API laods.
   */
  public function testApiClientWithCredentials(): void {
    $this->config('mailjet.settings')
      ->set('mailjet_active', TRUE)
      ->set('mailjet_username', 'username')
      ->set('mailjet_password', 'password')
      ->save();

    $mailjet_client = mailjet_new();
    $this->assertNotNull($mailjet_client);
    $this->assertInstanceOf(Client::class, $mailjet_client);
  }

  /**
   * Tests that the test API client does not load without credentials.
   */
  public function testApiClientWithoutCredentials(): void {
    $this->config('mailjet.settings')
      ->clear('mailjet_username')
      ->clear('mailjet_password')
      ->save();

    $this->assertNull(mailjet_new());
  }

  /**
   * Tests that the test API client does not load if it's not active.
   */
  public function testInactiveApiClient(): void {
    $this->config('mailjet.settings')
      ->set('mailjet_active', FALSE)
      ->set('mailjet_username', 'username')
      ->set('mailjet_password', 'password')
      ->save();

    $this->assertNull(mailjet_new());
  }

}
