<?php

namespace Drupal\Tests\mailjet\Functional;

/**
 * Contains tests for the domain add page.
 *
 * @group mailjet
 */
class DomainAddFormTest extends MailjetBrowserTestBase {

  /**
   * Tests setting form access for anonymous user.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testDomainFormAnonymousAccess(): void {
    $this->drupalLogout();
    $this->drupalGet($this->adminPathDomainAdd);
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Tests setting form access for authenticated (non-admin) user.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testDomainFormAuthenticatedAccess(): void {
    $this->drupalLogin($this->drupalCreateUser());
    $this->drupalGet($this->adminPathDomainAdd);
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Tests creating new domain via form.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testDomainFormAdmin(): void {
    // We should not have access to the page without saved credentials.
    $this->drupalLogin($this->drupalCreateUser(['administer mailjet configuration']));
    $this->drupalGet($this->adminPathDomainAdd);
    $this->assertSession()->statusCodeEquals(403);

    // Set API keys to be able to access settings pages.
    $config = $this->config('mailjet.settings');
    $config->set('mailjet_username', $this->randomString(32));
    $config->set('mailjet_password', $this->randomString(32));
    $config->set('mailjet_active', TRUE);
    $config->save();

    // Re-visit the page.
    $this->drupalGet($this->adminPathDomainAdd);
    $this->assertSession()->statusCodeEquals(200);

    // Try to save the domain (it will not work without real credentials).
    $this->submitForm([
      'domain' => $this->randomMachineName() . '.com',
    ], 'Add');
    $this->assertSession()->responseContains('Unauthorized');

    // Try to save an invalid domain.
    $this->submitForm([
      'domain' => 'non-valid-domain',
    ], 'Add');
    $this->assertSession()->responseContains('Please enter a valid domain name.');

  }

}
