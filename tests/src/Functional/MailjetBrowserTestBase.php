<?php

namespace Drupal\Tests\mailjet\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Sets up block content types.
 */
abstract class MailjetBrowserTestBase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['mailjet'];

  /**
   * A path to API keys configuration form.
   *
   * @var string
   */
  protected $adminPathKeys = 'admin/config/system/mailjet/api';

  /**
   * A path to mailjet configuration form.
   *
   * @var string
   */
  protected $adminPathDomainAdd = 'admin/config/system/mailjet/domains/add-domain';

}
