<?php

namespace Drupal\Tests\mailjet\Functional;

/**
 * Contains tests for the base API settings page.
 *
 * @group mailjet
 */
class ApiFormTest extends MailjetBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['mailjet'];

  /**
   * Tests setting form access for anonymous user.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testApiFormAnonymousAccess(): void {
    $this->drupalLogout();
    $this->drupalGet($this->adminPathKeys);
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Tests setting form access for authenticated (non-admin) user.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testApiFormAuthenticatedAccess(): void {
    $this->drupalLogin($this->drupalCreateUser());
    $this->drupalGet($this->adminPathKeys);
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Tests setting API credentials config via form.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testApiFormAdmin(): void {
    $this->drupalLogin($this->drupalCreateUser(['administer mailjet configuration']));
    $this->drupalGet($this->adminPathKeys);
    $this->assertSession()->statusCodeEquals(200);

    $key = $this->randomString(32);
    $secret = $this->randomString(32);

    $this->submitForm([
      'mailjet_username' => $key,
      'mailjet_password' => $secret,
    ], 'Save configuration');

    $config = $this->config('mailjet.settings');
    $this->assertEquals($key, $config->get('mailjet_username'));
    $this->assertEquals($secret, $config->get('mailjet_password'));
  }

}
