<?php

/**
 * @file
 * Path to composer.lock.
 */

$composerLockPath = __DIR__ . '/../../../composer.lock';

if (!file_exists($composerLockPath)) {
  echo "composer.lock not found. Cannot determine installed Drupal version.\n";
  exit(1);
}

// Parse composer.lock to find the installed Drupal core version.
$composerLock = json_decode(file_get_contents($composerLockPath), TRUE);
$packages = $composerLock['packages'] ?? [];
$drupalVersion = NULL;

foreach ($packages as $package) {
  if ($package['name'] === 'drupal/core-recommended') {
    $drupalVersion = $package['version'];
    break;
  }
}

if (!$drupalVersion) {
  echo "Drupal core-recommended package not found in composer.lock.\n";
  exit(1);
}

// Extract major version (e.g., 10 or 11)
$majorVersion = strtok($drupalVersion, '.');

// Determine the required symfony/validator version.
if ($majorVersion === '10') {
  $validatorVersion = '^6.3';
}
elseif ($majorVersion === '11') {
  $validatorVersion = '^7.0';
}
else {
  echo "Unsupported Drupal version: $drupalVersion\n";
  exit(1);
}

// Run composer require to adjust the symfony/validator version.
echo "Detected Drupal version: $drupalVersion\n";
echo "Requiring symfony/validator $validatorVersion...\n";

$command = sprintf('composer require symfony/validator:%s', escapeshellarg($validatorVersion));
exec($command, $output, $returnVar);

// Display the result of the composer command.
if ($returnVar === 0) {
  echo "Successfully required symfony/validator $validatorVersion.\n";
}
else {
  echo "Error requiring symfony/validator $validatorVersion:\n";
  echo implode("\n", $output) . "\n";
  exit(1);
}
