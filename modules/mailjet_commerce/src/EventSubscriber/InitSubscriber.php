<?php

namespace Drupal\mailjet_commerce\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Added mailjet campaign id to the session.
 */
class InitSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [KernelEvents::REQUEST => ['onEvent', 0]];
  }

  /**
   * Added mailjet campaign id to the session.
   */
  public function onEvent() {
    if (isset($_GET['token'])) {
      $_SESSION['mailjet_campaign_id'] = $_GET['token'];
    }
  }

}
