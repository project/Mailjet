<?php

namespace Drupal\mailjet_commerce\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Implement OrderComplete subscriber.
 *
 * @package Drupal\mailjet_commerce
 */
class OrderCompleteSubscriber implements EventSubscriberInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   */
  public function __construct(EntityTypeManager $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events['commerce_order.place.post_transition'] = ['orderCompleteHandler'];

    return $events;
  }

  /**
   * This method is called whenever the commerce_order.place.post_transition
   * event is dispatched.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   */
  public function orderCompleteHandler(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();

    if ($event->getToState()->getId() == 'completed') {

      if (isset($_SESSION['mailjet_campaign_id'])) {

        $campaign_mailjet_id = $_SESSION['mailjet_campaign_id'];

        $entity_type_manager = \Drupal::service('entity_type.manager');
        $order2 = $entity_type_manager->getStorage('commerce_order')
          ->load($order->getOrderNumber());
        $order2->set('field_mailjet_campaign_id', $campaign_mailjet_id);
        $order2->save();

        $query = \Drupal::database()->select('mailjet_campign', 'mj');
        $query->addField('mj', 'campaign_id');
        $query->condition('mj.camp_id_mailjet', trim($_SESSION['mailjet_campaign_id']));
        $query->range(0, 1);
        $id = $query->execute()->fetchField();

        $campaign = $entity_type_manager->getStorage('campaign_entity')
          ->load($id);

        $order2->set('field_mailjet_campaign_name', $campaign->get('name')
          ->getValue()[0]['value']);
        $order2->save();

        unset($_SESSION['mailjet_campaign_id']);
      }

    }

  }

}
