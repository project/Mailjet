<?php

namespace Drupal\mailjet_trigger_examples\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 *
 */
class TriggerExamplesController extends ControllerBase {

  /**
   *
   */
  public function content() {

    global $base_url;
    return mailjetExternalLinkRedirect($base_url . '/admin/structure/message');
  }

}
